;    Vezza: Z-Code interpreter for the Z80 processor
;    Z1/2/6/7 Extensions, bugfixes, rewrites and optimisations
;    Copyright (c) 2021-2025 Shawn Sijnstra <shawn@sijnstra.com>
;
;    Based on original ZXZVM:
;    Copyright (C) 1998-9,2006,2016  John Elliott <seasip.webmaster@gmail.com>
;    Portions copyright 2019 Garry Lancaster
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;pnmask:	defb	0	;Property number mask
;psmask:	defb	0	;Property size mask
;
;A lot of the code in this file has been hand-crafted from the C
;routines in jzip 2.0.1g.
;
;Find the property list for object DE
; destroys A
propadd:		;get_property_addr()
;;;	push	af
;	push	bc	;don't preserve - load it later in prop instructions
;	push	de
;	call	objadd	;preserves DE BC
	call	objadd_tsc	;preserves DE BC, always tested first.
;	ld	a,(zver)
;	ld	bc,7
;	cp	4
;	jr	c,propad3
propadd_fix:
	ld	de,12	;ver 4+. Changed to 7 for z1-z3 during load.
;propad3:
	add	hl,de
;o	ld	a,e
;	ld	e,0
;o	ld	e,b	;b is always 0 at this point.
	ld	e,d	;d is zero
	call	ZXPKWD	;BC = property pointer. Increment not required.
;o	ld	e,a
	ld	h,b
	ld	l,c	;HL = property pointer
;	call	ipeek	;Read length of text ; ZXPK64 + inc HL?
	call	ZXPK64rw
	inc		hl		;should be faster this way
;2023	ld	c,a		;4
;2023	ld	b,0
;2023	add	hl,bc	;11
;2023	add	hl,bc	;11 Skip over text
	ld	e,a		;d is still 0
	add	hl,de
	add	hl,de	;skips over text
;	pop	de
;	pop	bc
;;;	pop	af
	ret
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;HL->property, make it point at the next property
; references to propnxt replaced during load for versions 1-3
; both routines destroy A
propnxt:		;get_next_property()
;	push	bc
;;;	push	af
	call	ZXPK64rw	;Get property ID
	inc	hl		;inc built in now but might be SLOWER
;	ld	c,a
;	ld	a,(zver)
;	cp	4
;	jr	c,pnv3	;v1-3 property has size in top 3 bits
;	bit	7,c
	or	a
	jp	m,pnv4
;	jr	nz,pnv4
;	bit	6,c
	bit	6,a
	jr	z,pnv7
	inc	hl
pnv7:
;	jr	pnv7
	inc	hl
;;;	pop	af
	ret

pnv4:	call	ZXPK64rw	;Read property size

;	and	03Fh	;Size
;	jr	nz,pnv5
;	ld	a,40h	;Size of 0 means 64 bytes (standard 12.4.2.1.1)
;Experimental replacement - should work AND be faster
	dec	a
	and	03FH
	inc	a

;	jr	pnv5
;
;pnv3:	ld	a,c
;	and	0E0h
;	rlca
;	rlca
;	rlca		;A = length of property - 1
pnv5:
;	ld	c,a
;	ld	b,0
;	add	hl,bc
	inc	a	;instead of inc HL below, save 2 T-states
	add	a,l
	ld	l,a
	ret	nc		;11/5	- 11 or 19. Win both ways, save 2 bytes
	inc	h		;4
;pnv7:
;;	inc	hl
;;;	pop	af
;	pop	bc
	ret
;

propnxt_v3:
;	push	bc
;;;	push	af
	call	ZXPK64rw	;Get property ID
;	inc	hl		;moved into a to save 2 t-states
;	ld	c,a
;
;pnv3:	ld	a,c
	and	0E0h	; 1110 0000
	rlca
	rlca
	rlca		;A = length of property - 1
;pnv5:
;	ld	c,a
;	ld	b,0
;	add	hl,bc
	inc	a		;A = property length
	inc	a		;HL adjustment from above (plenty of head room)
	add	a,l
	ld	l,a
	ret	nc		;11/5	- 11 or 19. Win both ways, save 2 bytes
	inc	h		;4
;pnv7:
;;	inc	hl
;;;	pop	af
;	pop	bc
	ret			;10
;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;

;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; gplen for version 4+ & gplen3 for version 1-3
; psmask = 03Fh (4+), 0e0h (1-3)
; changed to return BC now
gplen:
;	ld	a,(psmask)	;from jzip, load_property_length()
;	ld	d,a
	dec	hl	;HL -> property ID
;	ld	a,(zver)
;	cp	4
;	jr	c,gplen3
	call	ZXPK64rw
;	ld	e,a	;spurious instruction?
;	bit	7,a
;	jr	z,gplen1
	or	a
	jp	p,gplen1	;fast check for sign bit
;	ld	a,e	;already equal
;;;	and	03fh	;psmask for z4+ ;d
;;;	jr	nz,rprop
;	ld	a,40h	;Size of 0 means 64 bytes (standard 12.4.2.1.1)
;	jr	rprop
;;;	ld	hl,0040h	;Size of 0 means 64 bytes (standard 12.4.2.1.1)
	dec	a			;should be faster for the same number of bytes
	and	03fH
	inc	a
;	ld	l,a
;	ld	h,0
	ld	c,a
	ld	b,0
	ret
;
gplen1:
;	ld	hl,2
	ld	bc,2
	bit	6,a
;	scf		;carry flag unused.
	ret	nz
;	dec	hl	;Doesn't touch flags
	dec	bc
;	scf
	ret

gplen3:
	dec	hl	;HL -> property ID
	call	ZXPK64rw	;Get property length in v1-v3

	and	0e0h	;psmask z1-3 ;d	;from the 3 high bits of the byte
	rlca
	rlca
	rlca
	inc	a
;rprop:
;	ld	l,a
;	ld	h,0
	ld	c,a
	ld	b,0
	ret
