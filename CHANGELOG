; 2025-02-09 - Shawn Sijnstra Release 8
; [+] Code optimisation for speed and size including
;		init, bwdcopy, fwdcopy, tokenize, inibuf, find_word, next_token, llf
; [+] Rewrote all core code to assume auto-increment of ZPC
;		to improve speed and size
; [+] Bugfix: handle auto wrapped line followed by immediate line feed
; [+] Additional space freed to allow for more game cache
; [+] Memory map reshuffled to reduce disk size. CP/M version 900+ bytes smaller
;		freeing up extra disk space.
;
; 2024-01-07 - Shawn Sijnstra release 7
; [+] Optimized binary size using code rewrite and self-modifying strings
; [+] Reduced file size by moving from zeroed ram to uninitialized
; [+] Used extra memory to increase cache by another 2-3 sectors
; [+] Minor speed improvement including call/return, object and compare
; [+] Bugfix: Some specific error messages were incorrectly generalized
; [+] Bugfix: In a rare edge case, disk full error was not handled properly
;
; 2023-07-16 - Shawn Sijnstra release 6
; [+] cache speedup and core speed rewrites in: stream 3, charout,
;		disptach (major rewrite)
;
; 2022-08-07 - Shawn Sijnstra release 5
; [+] Core code rewrites for speed in : parsing var/1op/2op, encode
;		dispatch, objects, attribute, properties, text buffer, stream3
; [+] Code rewrite for tracking exceptions/errors
; [+] Front end rewrite for speed in: cursor, text wrap, cache mem
; [+] Increased cache size by 3 sectors
; [+] Bugfix: timer routine fixed and scaled for M1/M3 interrupt differences
; [+] Bugfix: transcript support now works
; [+] Enhancement: extra space allows for Border Zone and Shogun on M1/3
;		FreHD or other non-Floppy system highly recommended - these games are
;		I/O intensive (as are others that have worked before - use your judgement)
;
; 2022-05-15 - Shawn Sijnstra release 4
; [+] Rewritten ZPC tracking for speed improvement
;       Courtesy of Garry Lancaster (zxzvm for Spectrum Next)
; [+] Bugfix: now processes requests for "Stream 0"
; [+] Core code speed rewrites for text encode, call, return, branch,
;		timed input, instruction decode
; [+] Rewrote memory management to improve speed
; [+] Multiple fixes to increase available cache
; [+] Synchronised core code 1/3/4
;
; 2022-04-24 - Shawn Sijnstra release 3
; [+] Fixed up arrow key mapping (fixes zsnake.z5)
; [+] Rewrote scrolling routines to fix cosmetics
; [+] Use native DOS error messages to improve error message meaning
; [+] Multiple speed improvements across core and routines
; [+] Improved memory usage to increase cache by 2 more sectors
; [+] Bugfixes
;
;
; 2022-04-08 - Shawn Sijnstra release 2
; [+] Allowed all DOS versions to load (tested with NewDos, LDOS, DosPlus....)
; [+] Improved status bar reverse space character and more prompt
; [+] z6 support reinstated (Arthur and unfinished games work)
; [+] Improved cache to be floating size, enabling FreHD/harddisk to work
; [+] Speed improvements for display, memory access
; [+] Game save/load rewritten to improve speed on real hardware
;       now uses LRL=256. Most DOS version ignore LRL, however, it can be
;       changed between 128 and 256 if reqiuired without affecting the contents.
;       refer to your DOS manual for specific commands.
