;    Vezza: Z-Code interpreter for the Z80 processor
;    Z1/2/6/7 Extensions, bugfixes, rewrites and optimisations
;    Copyright (c) 2021-2025 Shawn Sijnstra <shawn@sijnstra.com>
;
;    Based on original ZXZVM:
;    Copyright (C) 1998-9,2006,2016  John Elliott <seasip.webmaster@gmail.com>
;    Portions copyright 2019 Garry Lancaster
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
;	Burnable code area
;	EXECUTED only once to configure self-modifying code based on story version
;	and other story details.
;	note needed for a restart
;	can be overwritten by the running game stack
;
;/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;	THIS SETUP SHOULD ONLY HAPPEN ONCE, NOT DURING A RESTART
;
;Get g_addr, address of globals table; also strings & routines
;	THIS SETUP SHOULD ONLY HAPPEN ONCE, NOT DURING A RESTART
;	Move this to the burnable code area.
var_space:
Once_only:
	ld	e,0
	ld	hl,08h
	call	ZXPKWI
	ld	(d_addr),bc	;Dictionary
	call	ZXPKWI
;	ld	(obj_addr_1),bc	;Object Table (1st location) - moved to add ptlen offset for v1-3/v4+ versions
	ld	(obj_addr_2),bc	;Object Table (2nd location)
	call	ZXPKWI
;	ld	(g_addr),bc	;Global Variables
	ld	h,b
	ld	l,c
	ld	bc,0020h	;build in the offset
	and	a
	sbc	hl,bc		;so we don't need to sub 10
	ld	(g_addr),hl	;for all Global Variables
;
	ld	hl,18h		;Location of abbreviations table (byte address)
	call	ZXPKWD	;Doesn't need incrementing
	ld	(abbr_table),bc
;	
	ld	hl,28h		;Routines offset
	call	ZXPKWI
;	ld	(r_offs),bc	;v6/7 only - lets just store now instead of later.
	push	hl
	xor	a
	ld	h,b
	ld	l,c
	add	hl,hl		;multiply AHL by 2.
	rla
	ld	(upack_2R_O_E),a		;store result for quick calculation later
	ld	(upack_2R_O_HL),hl
	pop		hl
	call	ZXPKWI	;Static strings offset
;	ld	(s_offs),bc	;v6/7 only - lets just store now instead of later.
	xor	a	; only used by v6/7 but can calc anyway
	ld	h,b
	ld	l,c
	add	hl,hl		;multiply EHL by 2.
	rla
	ld	(upack_2S_O_E),a		;store result for quick calculation later
	ld	(upack_2S_O_HL),hl

;fix up dictionary encode to use shift not shift-lock for v1,v2
	ld	a,(zver)
	cp	3
	jr	nc,setup_unpack
	ex	af,af'	;save zver
	ld	a,2
	ld	(encfix2+1),a
	inc	a
	ld	(encfix3+1),a
;character table fixes for V1 only (Use AL2a instead of AL2)
;	ld	a,(zver)
	ex	af,af'	;restore zver - faster, saves a byte
	dec	a
	jr	nz,setup_unpackx
	ld	hl,al2+8	;update al2 into al2a
	ld	de,al2+7	;16 bytes
	ld	bc,20
	ldir
;	ld	a,'<'
;	ld	(al2_end-5),a
	ld	hl,al2_end-5
	ld	(hl),'<'
setup_unpackx:
	inc	a
;Work out which upack_addr to use
; expanded to separate R_O and S_O to support z6/z7

setup_unpack:
;	ld	a,(zver)
	ld	l,a
	ld	h,0
	add	hl,hl
	ld	de,upack_table_R_O
	add	hl,de
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	ld	(upack_addr_R_O_34+1),de
	ld	(upack_addr_R_O_5+1),de

	ld	de,upack_table_S_O - upack_table_R_O - 1	;offset due inc HL above.
	add	hl,de
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	ld	(upack_addr_S_O+1),de	
;
;Compute property sizes and object sizes
;
;	ld	a,(zver)	;a still has zver
	cp	5		;these changes are for versions 1-4
	jr	nc,property_sizes
	ld	hl,call_g1_34	;version 3/4 routine call routine group is different
	ld	(call_fix),hl
	ld	hl,v1pop
	ld	(zero_ops+(9*2)),hl	;replace catch with v1pop
	ld	hl,znot
	ld	(one_ops+(0fh*2)),hl
	ld	hl,fail
	ld	(ext_inst_fix),hl
property_sizes:
	cp	4		;a still has zver
	jr	c,psv3		;default values in place are for v4+
	ld	bc,126		;property table length
	ld	hl,(obj_addr_2)
	add	hl,bc		;no longer need obj_addr_1 because we add offset now.
	ld	(ptloc+1),hl
IFDEF	V6routines
;	ld	a,(zver)
	cp	6
	jr	nz,psv_not6	;special v6-only changes
	ld	hl,zpull_v6
	ld	(var_ops+(09h*2)),hl
	ld	hl,setcur_v6
	ld	(var_ops+(0Fh*2)),hl
psv_not6:
ENDIF
;	scf
;	ret
	jp	psv4

psv3:
	ld	a,0E0h
	ld	(psmask_fix1+1),a
	ld	(psmask_fix2+1),a
;	ld	a,01Fh
	cpl
	ld	(pnmask_fix1+1),a
	ld	(pnmask_fix2+1),a
	ld	(pnmask_fix3+1),a
	ld	(pnmask_fix4+1),a
	ld	(pnmask_fix5+1),a
;	ld	hl,9
;	ld	(objlen+1),hl
;;	ld	a,9		;changed - equivalent is below in times14or9
;;	ld	(objlen+1),a
	ld	bc,62		;Property table length
	ld	hl,(obj_addr_2)
	add	hl,bc		;no longer need obj_addr_1 because we add offset now.
	ld	(ptloc+1),hl
;adjust code + offsets for objects to be v1-v3 version
;	ld	hl,4	;faster and 1 byte smaller with a.
	ld	a,4
	ld	(write_parent+1),a
	ld	(read_parent+1),a
	inc	a
	ld	(write_sibling+1),a
	ld	(read_sibling+1),a
	inc	a
	ld	(write_child+1),a
	ld	(read_child+1),a
	inc	a		;7
	ld	(propadd_fix+1),a
	ld	(probj_fix+1),a

	ld	hl,owriteb
	ld	(write_parent+5),hl
	ld	(write_sibling+5),hl
	ld	(write_child+5),hl
	ld	hl,oreadb
	ld	(read_parent+4),hl
	ld	(read_sibling+4),hl
	ld	(read_child+4),hl

	ld	hl,v3jin
	ld	(two_ops+(06*2)),hl

	ld	hl,gplen3
	ld	(gplen_fix+1),hl

IFDEF	v3Split
	ld	hl,split_w3
	ld	(var_ops+(0Ah*2)),hl
ENDIF
	ld	hl,sel_w3
	ld	(var_ops+(0Bh*2)),hl

	ld	hl,propnxt_v3
	ld	(pnv3fix_1),hl
	ld	(pnv3fix_2),hl
	ld	(pnv3fix_3),hl
	ld	(pnv3fix_4),hl
	ld	(pnv3fix_5),hl

	ld	hl,gpad5
	ld	(gpadfix),hl

;	ld	hl,cpenc1
;	ld	(cpenc_fix1),hl
;	ld	(cpenc_fix2),hl
	ld	hl,cpenc_fix
	ld	(hl),0c9h	;replace ret nz with ret

	xor	a
	ld	(times14or9),a	;zero out add hl,bc
;	ld	(times14or9+2),a
	ld	hl,0929h	;and swap add hl,hl and add hl,bc
	ld	(times14or9+2),hl	;to turn *14 into *9

psv4:
; Confirm that Z-machine memory is writable.
;
;

test_mem:
	ld	hl,40h
	call	ZXPK64rw
	ld	d,a	;Correct value
	cpl
	ld	e,a	;Different value
	call	ZXPOKE
	call	ZXPK64rw
	cp	e	;Has the change registered?
	jr	nz,tm_fail
	ld	a,d	;Change back to the correct value.
	call	ZXPOKE
	call	ZXPK64rw
	cp	d	;Has the change back registered?
	scf
	ret	z
;
tm_fail:
	ld	hl,memerr
	xor	a		;failed to use memory
	ret

; 18 x 3 bytes below = 54 bytes. moved to save buffer to save space.
;memerr:	defb	'Memory test faile'
;	defb	0E4h		;'d'+80h
;
;IFDEF	V6routines
;upack_table_R_O:
;	defw	fail
;	defw	upack1, upack2, upack3, upack4
;	defw	upack5, upack6R, upack7R, upack8
;upack_table_S_O:
;	defw	fail
;	defw	upack1, upack2, upack3, upack4
;	defw	upack5, upack6S, upack7S, upack8
;ELSE
;upack_table_R_O:
;	defw	fail
;	defw	upack1, upack2, upack3, upack4
;	defw	upack5, fail, upack7R, upack8
;upack_table_S_O:
;	defw	fail
;	defw	upack1, upack2, upack3, upack4
;	defw	upack5, fail, upack7S, upack8
;	ENDIF

endburn	EQU	$
;